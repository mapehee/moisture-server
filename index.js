var express = require('express')
var app = express();
var path = require('path');
var url = require('url');
var fs = require('fs');


app.use('/public', express.static(path.join(__dirname, 'public')))

app.get('/', function (req, res) {
    if(!req.query.id){res.send("null");}
    if(req.query.stage && req.query.mood){
        stage = req.query.stage;
        mood = req.query.mood;

        if(Number(req.query.id)==1){
            stage = "seed";
        }
        if(Number(req.query.id)==2){
            stage = "baby";
            mood = "good";
        }
        if(Number(req.query.id)==3){
            stage = "child";
            mood = "good";
        }
        if(Number(req.query.id)==4){
            stage = "adult";
        }

        fs.writeFile('status'+req.query.id+'.dat', JSON.stringify({stage:stage, mood:mood}),    function (err) {
            if (err) throw err;
          });
        res.send("ok");
        }
    else{
        fs.readFile("status"+req.query.id+".dat", function(err,contents){
            obj = JSON.parse(contents);
            res.send(obj.stage+"/"+obj.mood)
        });
    }
});

app.listen(3000, function () {
})


